import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { signupRequest } from 'store/actions/auth';
import Spinner from 'components/Spinner/Spinner';
import Alert from 'components/Alert/Alert';

class SignupContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: ''
    }

    this.firstNameInput = React.createRef();
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { firstName, lastName, email, password, confirmPassword } = this.state;
    const { signup } = this.props;
    signup(firstName, lastName, email, password, confirmPassword);
  }

  componentDidMount() {
    this.firstNameInput.current.focus();
  }

  render() {
    const { firstName, lastName, email, password, confirmPassword } = this.state;
    const isValidForm = firstName &&
      lastName &&
      email &&
      password &&
      confirmPassword &&
      password.length >= 4 &&
      password === confirmPassword;
    const signupError = this.props.signupError;
    let signupErrorMessage = "";
    if (signupError) {
      signupErrorMessage = signupError.response.data.email[0];
    }
    return (
      <div className="container py-3">
        <div className="col-md-5 mx-auto card shadow">
          <div className="card-body">
            <h4 className="card-title">Sign up</h4>
            <hr />
            {
              signupErrorMessage.length > 0 &&
              <Alert type="danger">
                This email has been used.
              </Alert>
            }
            {
              this.props.signupSuccess &&
              <Alert type="success">
                You have successfully created your account.
              </Alert>
            }
            <form onSubmit={this.handleSubmit}>
              <div className="d-flex flex-column flex-lg-row">
                <div className="form-group mr-lg-3 flex-md-grow-1">
                  <label htmlFor="first-name">First name</label>
                  <input
                    type="text"
                    name="firstName"
                    id="first-name"
                    className="form-control"
                    onChange={this.handleChange}
                    ref={this.firstNameInput}
                    required />
                </div>
                <div className="form-group flex-grow-1">
                  <label htmlFor="last-name">Last name</label>
                  <input
                    type="text"
                    name="lastName"
                    id="last-name"
                    className="form-control"
                    onChange={this.handleChange}
                    required />
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  name="email"
                  id="email"
                  className="form-control"
                  onChange={this.handleChange}
                  required />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                  type="password"
                  name="password"
                  id="password"
                  className="form-control"
                  onChange={this.handleChange}
                  required />
              </div>
              <div className="form-group">
                <label htmlFor="confirm-password">Confirm password</label>
                <input
                  type="password"
                  name="confirmPassword"
                  id="confirm-password"
                  className="form-control"
                  onChange={this.handleChange}
                  required />
              </div>
              <p className="text-center">If you already have an account, just <Link to="/log-in">sign in</Link></p>
              <button
                type="submit"
                className="btn btn-primary mx-auto d-block"
                disabled={!isValidForm}>
                <Spinner show={this.props.signupPending} />
                Sign up
                </button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  signupPending: state.auth.signupPending,
  signupError: state.auth.signupError,
  signupSuccess: state.auth.signupSuccess
});

const mapDispatchToProps = dispatch => ({
  signup: (firstName, lastName, email, password, confirmPassword) => dispatch(signupRequest(firstName, lastName, email, password, confirmPassword))
});

export default connect(mapStateToProps, mapDispatchToProps)(SignupContainer);