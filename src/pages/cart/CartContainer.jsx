import React from 'react';
import withPrivateComponent from 'hocs/withPrivateComponent';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import CartItem from 'components/CartItem/CartItem';
import { removeFromCart, updateCart } from 'store/actions/cart';
import { faCreditCard, faShoppingBasket } from '@fortawesome/free-solid-svg-icons';

class CartContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      deleteId: null
    }
  }

  handleDecrement = (productId, amount) => {
    if (amount > 0) {
      const { token, updateCart } = this.props;
      updateCart(token, productId, amount);
    }
  }

  handleIncrement = (productId, amount) => {
    const { token, updateCart } = this.props;
    updateCart(token, productId, amount);
  }

  handleRemoveItem = (productId) => {
    const confirmation = window.confirm("Do you want to delete this product?");
    if (!confirmation) return;

    const { token, removeFromCart } = this.props;
    this.setState({
      deleteId: productId
    });
    removeFromCart(token, productId);
  }


  render() {
    const { cart } = this.props;
    let details = [];
    if (cart) {
      details = cart.cart_detail;
    }

    return (
      <div className="container-fluid py-3">
        <h4>Cart details</h4>
        <hr />
        {
          details && details.length ?
            <>
              {
                // Cart items
                details.map((detail, index) => {
                  return (
                    <CartItem
                      deletePending={this.props.removeFromCartPending && this.state.deleteId === detail.product.id}
                      updateCartPending={this.props.updateCartPending}
                      onRemoveItem={this.handleRemoveItem}
                      onDecrement={this.handleDecrement}
                      onIncrement={this.handleIncrement}
                      detail={detail}
                      key={index} />
                  )
                })
              }
              {/* Cart total */}
              <div className="row position-sticky bg-success py-2 mx-1 rounded shadow text-white" style={{ bottom: 0, zIndex: 1020 }}>
                <div className="col-10">
                  <h4>Total</h4>
                </div>
                <div className="col-2">
                  <h4>${cart.total}</h4>
                </div>
              </div>

              {/* Cart buttons */}
              <div className="row text-right mt-3">
                <div className="col">
                  <Link className="btn btn-secondary" to="/">
                    <FontAwesomeIcon icon={faShoppingBasket} />
                    <span className="ml-2">Continue buying</span>
                  </Link>
                  <Link className="btn btn-primary ml-3" to="/check-out">
                    <FontAwesomeIcon icon={faCreditCard} />
                    <span className="ml-2">Checkout</span>
                  </Link>
                </div>
              </div>
            </> :
            <p>Please go to <Link to="/">homepage</Link> to buy some cool products...</p>
        }
      </div>
    );
  };
};

const mapStateToProps = state => ({
  user: state.user.user,
  token: state.auth.token,
  cart: state.cart.cart,
  removeFromCartPending: state.cart.removeFromCartPending,
  updateCartPending: state.cart.updateCartPending
});

const mapDispatchToProps = dispatch => ({
  removeFromCart: (token, productId) => dispatch(removeFromCart(token, productId)),
  updateCart: (token, productId, amount) => dispatch(updateCart(token, productId, amount))
})

export default connect(mapStateToProps, mapDispatchToProps)(withPrivateComponent(CartContainer));