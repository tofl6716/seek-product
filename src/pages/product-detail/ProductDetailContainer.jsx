import React from 'react';
import { connect } from 'react-redux';
import { NavLink, withRouter } from 'react-router-dom';
import Loading from 'components/Loading/Loading';
import { fetchProductDetails } from 'store/actions/product';
import ProductList from 'components/ProductList/ProductList';
import { addToCart } from 'store/actions/cart';

import './ProductDetail.css';
import Spinner from 'components/Spinner/Spinner';
import AmountCounter from 'components/AmountCounter/AmountCounter';

class ProductDetailContainer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      amount: 1
    }
  }

  handleAmountChange = (e) => {
    if (e.target.value.match(/[^0-9]/g))
      return false;
    this.setState({
      amount: e.target.value
    })
  }

  handleDecrement = () => {
    this.setState((prevState) => {
      if (prevState.amount > 1) {
        return {
          amount: prevState.amount - 1
        };
      }
    })
  }

  handleIncrement = () => {
    this.setState((prevState) => ({
      amount: prevState.amount + 1
    }))
  }

  componentDidUpdate(prevProps) {
    const imgs = document.querySelector('#product-details img');
    if (imgs) {
      imgs.classList.add('img-fluid');
    }

    if (prevProps.addToCartSuccess !== this.props.addToCartSuccess && !this.props.addToCartPending) {
      alert("Added to cart");
    }
  }

  componentDidMount() {
    const { fetchProductDetails, match } = this.props;
    const productId = match.params.productId;
    if (productId) {
      fetchProductDetails(productId);
    }
  }

  handleAddToCart = (productId, amount) => {
    const { user, history, location, token, addToCart } = this.props;
    if (!user) {
      history.push({
        pathname: "/sign-in",
        state: {
          from: location
        }
      });
    } else {
      const confirmation = window.confirm(`Do you want to add ${amount} to cart?`);
      if (!confirmation) return;

      addToCart(token, productId, amount);
    }
  }

  render() {
    const { productDetails } = this.props;
    let product = null;
    let relatedProducts;
    if (productDetails) {
      product = productDetails.product;
      const otherProducts = productDetails.other_products
      relatedProducts = otherProducts.splice(0, 4);
    }
    return (
      productDetails ?
        <div className="container-fluid mt-3">
          <h3 className="text-truncate text-primary">Product details</h3>
          <hr />
          <div className="row">
            <div className="col-sm-4">
              <img className="img-fluid img-thumbnail" src={product.image || 'https://via.placeholder.com/400x300'} alt="" />
            </div>
            <div className="col-sm-8">
              <h4><strong>{product.name}</strong></h4>
              <h3 className="text-danger"><strong>$ {product.price}</strong></h3>
              <p>{product.description}</p>
              <p>Model: {product.model}</p>
              <p>Category: {
                product.category.map(c => {
                  return <NavLink key={c.id} to={`/${c.name}`}>{c.name}</NavLink>
                })
              }</p>
              <p>In stock: {product.in_stock}</p>

              <div className="row">
                <div className="col-sm-3">
                  <AmountCounter
                    change={this.handleAmountChange}
                    decrement={this.handleDecrement}
                    increment={this.handleIncrement}
                    value={this.state.amount}
                  />
                </div>
                <button
                  className="btn btn-primary col-sm-3"
                  onClick={() => this.handleAddToCart(product.id, this.state.amount)}>
                  <strong>
                    <Spinner show={this.props.addToCartPending} />
                    ADD TO CART
                    </strong>
                </button>
              </div>
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col" id="product-details" dangerouslySetInnerHTML={{ __html: product.full_description }}></div>
          </div>

          <div className="row">
            <div className="col">
              <p>Tags: </p>
            </div>
          </div>
          <h3 className="text-primary">Related products</h3>
          <hr />
          <ProductList products={relatedProducts} />
        </div> :
        <Loading />
    )
  }
}

const mapStateToProps = state => ({
  token: state.auth.token,
  user: state.user.user,
  productDetails: state.product.productDetails,
  pending: state.product.productDetailsPending,
  error: state.product.productDetailsError,
  addToCartPending: state.cart.addToCartPending,
  addToCartError: state.cart.addToCartError,
  addToCartSuccess: state.cart.addToCartSuccess
});

const mapDispatchToProps = dispatch => ({
  fetchProductDetails: (productId) => dispatch(fetchProductDetails(productId)),
  addToCart: (token, productId, amount) => dispatch(addToCart(token, productId, amount))
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProductDetailContainer));