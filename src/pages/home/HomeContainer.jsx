import React from "react";
import { connect } from 'react-redux';
import CategoryList from "components/CategoryList/CategoryList";
import ProductList from "components/ProductList/ProductList";
import fetchCategories from 'store/actions/category';
import { fetchRandomProducts, fetchProductsByCategory } from 'store/actions/product';
import Loading from "components/Loading/Loading";

class HomeContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: [],
      products: []
    };
  }

  fetchProductsByRoute() {
    const { match, fetchRandomProducts, fetchProductsByCategory } = this.props;
    if (match.url === '/') {
      fetchRandomProducts();
    } else if (match.params.category) {
      fetchProductsByCategory(match.params.category);
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.url !== this.props.match.url) {
      this.fetchProductsByRoute();
      window.scrollTo(0, 0);
    }
  }

  async componentDidMount() {
    this.props.fetchCategories();
    this.fetchProductsByRoute();
  }

  render() {
    return (
      <>
        {
          this.props.pending ?
            <Loading /> :
            <div className="container-fluid">
              <div className="d-flex">
                <CategoryList categories={this.props.categories} />
                <ProductList products={this.props.products} />
              </div>
            </div>
        }
      </>
    );
  }
};

const mapStateToProps = state => {
  return {
    categories: state.category.categories,
    products: state.product.products,
    pending: state.category.pending || state.product.pending,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchCategories: () => dispatch(fetchCategories()),
    fetchRandomProducts: () => dispatch(fetchRandomProducts()),
    fetchProductsByCategory: (category) => dispatch(fetchProductsByCategory(category))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);