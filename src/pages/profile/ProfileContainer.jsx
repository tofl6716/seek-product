import React from 'react';
import withPrivateComponent from 'hocs/withPrivateComponent';
import { connect } from 'react-redux';
import { updateUserProfile } from 'store/actions/user';
import Spinner from 'components/Spinner/Spinner';

class ProfileContainer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      originalImage: '',
      avatar: null
    }

    this.firstNameInput = React.createRef();
    this.lastNameInput = React.createRef();
    this.phoneInput = React.createRef();
    this.workatInput = React.createRef();
    this.jobInput = React.createRef();
    this.showPhoneCheck = React.createRef();
    this.avatarImage = React.createRef();
    this.messageInput = React.createRef();
  }

  handleUploadImage = (e) => {
    e.preventDefault();
    alert("Can not use this feature because of API errors!");
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { updateUserProfile, token } = this.props;
    const body = {
      first_name: this.firstNameInput.current.value,
      last_name: this.lastNameInput.current.value,
      phone_number: this.phoneInput.current.value,
      work_at: this.workatInput.current.value,
      job: this.jobInput.current.value,
      show_phone: this.showPhoneCheck.current.checked,
      message: this.messageInput.current.value,
    }
    updateUserProfile(token, body);
  }

  componentDidUpdate(prevProps) {
    if (this.props.user !== prevProps.user) {
      alert('Profile updated');
      window.scrollTo(0, 0);
    }
  }

  render() {
    const { user } = this.props;
    return (
      <div className="container py-3">
        <div className="row">
          <form className="col-sm-6 mx-auto shadow p-5 rounded-lg" onSubmit={this.handleSubmit}>
            <h1 className="text-danger">User profile</h1>
            <hr />
            <div className="form-group row">
              <label htmlFor="avatar" className="col-form-label col-sm-4">Avatar</label>
              <div className="col-sm-8 text-center">
                <img ref={this.avatarImage} width="200" height="200" className="img-fluid img-thumbnail" src={user.avatar || 'https://via.placeholder.com/200x200'} alt="" />
                <label className="btn btn-success mt-2 cursor-pointer">
                  Update avatar
                  <input type="file" onChange={this.handleUploadImage} hidden/>
                </label>
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="email" className="col-form-label col-sm-4">Email</label>
              <div className="col-sm-8">
                <input type="text" value={user.email} id="email" className="form-control" readOnly />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="first-name" className="col-form-label col-sm-4">First name</label>
              <div className="col-sm-8">
                <input type="text" defaultValue={user.first_name} ref={this.firstNameInput} name="first_name" id="first-name" className="form-control" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="last-name" className="col-form-label col-sm-4">Last name</label>
              <div className="col-sm-8">
                <input type="text" defaultValue={user.last_name} ref={this.lastNameInput} name="last_name" id="last-name" className="form-control" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="phone" className="col-form-label col-sm-4">Phone number</label>
              <div className="col-sm-8">
                <input type="text" defaultValue={user.phone_number || ""} ref={this.phoneInput} name="phone_number" id="phone" className="form-control" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="show-phone" className="col-form-label col-sm-4">Show phone</label>
              <div className="col-sm-8 d-flex align-items-center">
                <div className="custom-control custom-checkbox">
                  <input type="checkbox" defaultChecked={user.show_phone} ref={this.showPhoneCheck} className="custom-control-input" id="show-phone" />
                  <label className="custom-control-label" htmlFor="show-phone"></label>
                </div>
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="work-at" className="col-form-label col-sm-4">Work at</label>
              <div className="col-sm-8">
                <input type="text" defaultValue={user.work_at || ""} ref={this.workatInput} name="work_at" id="work-at" className="form-control" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="job" className="col-form-label col-sm-4">Job</label>
              <div className="col-sm-8">
                <input type="text" name="job" defaultValue={user.job || ""} ref={this.jobInput} id="job" className="form-control" />
              </div>
            </div>
            <div className="form-group row">
              <label className="col-form-label col-sm-4" htmlFor="message">Message:</label>
              <div className="col-sm-8">
                <textarea defaultValue={user.message} ref={this.messageInput} rows="3" id="message" className="form-control"></textarea>
              </div>
            </div>
            <hr />
            <div className="form-group row">
              <div className="col-sm-8 offset-sm-4">
                <button type="submit" className="btn btn-primary"><Spinner show={this.props.updatePending} /> Update profile</button>
                <button type="reset" className="btn btn-outline-secondary ml-2">Cancel</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  updatePending: state.user.updatePending,
  token: state.auth.token
});

const mapDispatchToProps = dispatch => ({
  updateUserProfile: (token, body) => dispatch(updateUserProfile(token, body))
});

export default connect(mapStateToProps, mapDispatchToProps)(withPrivateComponent(ProfileContainer));