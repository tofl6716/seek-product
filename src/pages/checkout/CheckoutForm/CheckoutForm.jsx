import React from 'react';
import PropTypes from 'prop-types';
import { injectStripe, CardNumberElement, CardCVCElement, CardExpiryElement } from 'react-stripe-elements';

import './CheckoutForm.css';
import Alert from 'components/Alert/Alert';
import Spinner from 'components/Spinner/Spinner';

const CheckoutForm = (props) => {
  return (
    <form
      className="rounded shadow px-3 py-4"
      onSubmit={(event) => props.checkout(event, props.stripe)}>

      <h3>Payment infomation</h3>
      <hr />

      {
        props.errorMessage.length > 0 &&
        <Alert type="danger">
          {props.errorMessage}
        </Alert>
      }

      {
        props.success &&
        <Alert type="success">
          Payment successfully, you will be redirected in 5 seconds
        </Alert>
      }

      <div className="form-group">
        <label htmlFor="card-number">Card number</label>
        <CardNumberElement id="card-number" />
      </div>
      <div className="form-group">
        <label htmlFor="exp-date">Expiration date</label>
        <CardExpiryElement id="exp-date" />
      </div>
      <div className="form-group mb-4">
        <label htmlFor="cvc">CVC</label>
        <CardCVCElement id="cvc" />
      </div>

      <div className="form-group text-right">
        <button type="button" className="btn btn-outline-danger">Canel</button>
        <button
          type="submit"
          className="btn btn-primary ml-2"
          disabled={props.paymentPending}>
            <Spinner show={props.paymentPending} />
            {
              props.paymentPending ?
              'Processing...' : 'Pay'
            }
          </button>
      </div>
    </form>
  )
}

CheckoutForm.propTypes = {
  checkout: PropTypes.func.isRequired,
  errorMessage: PropTypes.string,
  success: PropTypes.bool,
  paymentPending: PropTypes.bool
}

export default injectStripe(CheckoutForm);
