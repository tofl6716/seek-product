import React from 'react';
import { Elements } from 'react-stripe-elements';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import CheckoutForm from './CheckoutForm/CheckoutForm';
import withPrivateComponent from 'hocs/withPrivateComponent';
import { cartPayment } from 'store/actions/cart';

class CheckoutContainer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      errorMessage: ''
    };
  }

  payment = (userToken, stripeToken) => {
    const { cartPayment } = this.props;
    cartPayment(userToken, stripeToken);
  }

  handleSubmit = (event, stripe) => {
    event.preventDefault();
    const { createToken } = stripe;
    this.setState({ errorMessage: '' });

    createToken({ type: 'card' })
      .then(res => {
        if (res.token) {
          const stripeToken = res.token.id;
          const token = this.props.token;
          this.payment(token, stripeToken);

        } else if (res.error) {
          this.setState({ errorMessage: res.error.message });
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  componentDidUpdate() {
    const { paymentSuccess, history } = this.props;
    if (paymentSuccess) {
      setTimeout(() => {
        history.push("/");
      }, 5000);
    }
  }

  render() {
    return (
      <div className="container pt-3">
        <div className="row">
          <div className="col-sm-6 mx-auto">
            <Elements>
              <CheckoutForm
                success={this.props.paymentSuccess}
                errorMessage={this.state.errorMessage}
                checkout={this.handleSubmit}
                paymentPending={this.props.paymentPending} />
            </Elements>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  token: state.auth.token,
  cart: state.cart.cart,
  paymentPending: state.cart.paymentPending,
  paymentError: state.cart.paymentError,
  paymentSuccess: state.cart.paymentSuccess
});

const mapDispatchToProps = dispatch => ({
  cartPayment: (userToken, stripeToken) => dispatch(cartPayment(userToken, stripeToken))
})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withPrivateComponent(CheckoutContainer)));