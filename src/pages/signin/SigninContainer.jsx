import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Alert from 'components/Alert/Alert';
import { signinRequest } from 'store/actions/auth';
import Spinner from 'components/Spinner/Spinner';

class SigninContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      valid: false,
      errorMessage: ''
    }

    this.emailInput = React.createRef();
  }


  validateEmail = () => {
    const { email } = this.state;
    if (!email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/ig) && email.length > 0) {
      this.setState({
        errorMessage: 'Please enter a valid email'
      });
    } else {
      this.setState({
        errorMessage: ''
      });
    };
  };


  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };


  handleSubmit = (event) => {
    event.preventDefault();
    const { email, password } = this.state;
    const { signin } = this.props;
    signin(email, password);
  };


  componentDidUpdate(prevProps) {
    const { user, location, history } = this.props;
    if (user !== prevProps.user) {
      if (location.state) {
        history.push(location.state.from);
      } else {
        history.push("/");
      }
    }
  }

  componentDidMount() {
    // Focus to email field
    this.emailInput.current.focus();
  }


  render() {
    // Validate form to disable submit button
    const isValidForm = this.state.email && this.state.password && this.state.errorMessage.length === 0;

    // Handle errors from login API in redux
    let loginError = "";
    const { signinError } = this.props;
    if (signinError) {
      const loginErrorKey = Object.keys(signinError.data);
      const firstError = signinError.data[loginErrorKey[0]];
      loginError = firstError[0];
    }

    return (
      <div className="container py-3">
        <div className="col-md-5 mx-auto card shadow">
          <div className="card-body">
            <h4 className="card-title">Sign in</h4>
            <p><small>Sign in with your account</small></p>
            <hr />
            <form onSubmit={this.handleSubmit}>
              {
                // This alert handles form errors
                this.state.errorMessage.length > 0 &&
                <Alert type="danger">
                  {this.state.errorMessage}
                </Alert>
              }


              {
                // This alert handles user login errors
                loginError.length > 0 &&
                <Alert type="danger">
                  {loginError}
                </Alert>
              }

              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  name="email"
                  id="email"
                  className="form-control"
                  onBlur={this.validateEmail}
                  onChange={this.handleChange}
                  ref={this.emailInput} />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                  type="password"
                  name="password"
                  id="password"
                  className="form-control"
                  onChange={this.handleChange} />
              </div>
              <p className="text-center">If you don't have account, <Link to="/sign-up">create a new one</Link></p>
              <button
                type="submit"
                className="btn btn-primary mx-auto d-block"
                disabled={!isValidForm || this.props.signinPending || this.props.fetchUserPending}>
                <Spinner show={this.props.signinPending || this.props.fetchUserPending} />
                Sign in
            </button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.user.user,
    signinError: state.auth.signinError,
    signinPending: state.auth.signinPending,
    fetchUserPending: state.user.fetchPending
  }
}

const mapDispatchToProps = dispatch => {
  return {
    signin: (email, password) => dispatch(signinRequest(email, password))
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SigninContainer));