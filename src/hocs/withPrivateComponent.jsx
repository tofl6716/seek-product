import React from 'react';
import { Redirect } from 'react-router-dom';

const withPrivateComponent = ContainerComponent => {
  return class extends React.Component {
    render() {
      const { user, location } = this.props;
      return (
        user ?
        <ContainerComponent {...this.props} /> :
        <Redirect to={{
          pathname: "/sign-in",
          state: { from: location }
        }} />
      )
    };
  };
};

export default withPrivateComponent;