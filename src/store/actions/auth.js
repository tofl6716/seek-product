import * as actionTypes from "./actionTypes";
import { fetchUserProfile, removeUser } from "./user";
import { signin, signout, signup } from "api/auth";
import { fetchCart } from "./cart";

// SIGN IN FUNCTIONALITY

const signinRequestPending = () => {
  return {
    type: actionTypes.SIGNIN_PENDING
  };
};

const signinRequestSuccess = data => {
  return {
    type: actionTypes.SIGNIN_SUCCESS,
    payload: data
  };
};

const signinRequestFailure = error => {
  return {
    type: actionTypes.SIGNIN_FAILURE,
    payload: error
  };
};

export const signinRequest = (email, password) => {
  return dispatch => {
    dispatch(signinRequestPending());
    signin(email, password)
      .then(res => {
        const token = res.data.token;
        dispatch(signinRequestSuccess(token));
        dispatch(fetchUserProfile(token));
        dispatch(fetchCart(token));
      })
      .catch(error => {
        dispatch(signinRequestFailure(error.response));
      });
  };
};

// SIGN OUT FUNCTIONALITY

const signoutRequestPending = () => {
  return {
    type: actionTypes.SIGNOUT_PENDING
  };
};

const signoutRequestSuccess = () => {
  return {
    type: actionTypes.SIGNOUT_SUCCESS
  };
};

const signoutRequestFailure = error => {
  return {
    type: actionTypes.SIGNOUT_FAILURE,
    payload: error
  };
};

export const signoutRequest = token => {
  return dispatch => {
    dispatch(signoutRequestPending());
    signout(token)
      .then(res => {
        if (res.data.message === "logout success") {
          dispatch(removeUser());
          dispatch(signoutRequestSuccess());
        }
      })
      .catch(error => {
        dispatch(signoutRequestFailure(error));
      });
  };
};

// Sign up

const signupRequestPending = () => {
  return {
    type: actionTypes.SIGNUP_PENDING
  };
};

const signupRequestSuccess = () => {
  return {
    type: actionTypes.SIGNUP_SUCCESS
  };
};

const signupRequestFailure = error => {
  return {
    type: actionTypes.SIGNUP_FAILURE,
    payload: error
  };
};

export const signupRequest = (
  firstName,
  lastName,
  email,
  password,
  confirmPassword
) => {
  return dispatch => {
    dispatch(signupRequestPending());
    // Call API
    signup(firstName, lastName, email, password, confirmPassword)
      .then(res => {
        if (res.data.id && res.data.email === email) {
          dispatch(signupRequestSuccess());
        }
      })
      .catch(error => {
        dispatch(signupRequestFailure(error));
      });
  };
};

// SET TOKEN
export const setToken = token => {
  return {
    type: actionTypes.SET_TOKEN,
    payload: token
  };
};
