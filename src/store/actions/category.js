import { getCategoriesFromAPI } from "api/category";
import * as actionTypes from "./actionTypes";

const fetchCategoriesPending = () => {
  return {
    type: actionTypes.FETCH_CATEGORIES_PENDING
  };
};

const fetchCategorySuccess = categories => {
  return {
    type: actionTypes.FETCH_CATEGORIES_SUCCESS,
    payload: categories
  };
};

const fetchCategoryFailure = error => {
  return {
    type: actionTypes.FETCH_CATEGORIES_FAILURE,
    payload: error
  };
};

const fetchCategories = () => {
  return dispatch => {
    dispatch(fetchCategoriesPending());
    getCategoriesFromAPI()
      .then(res => {
        dispatch(fetchCategorySuccess(res.data));
      })
      .catch(error => {
        dispatch(fetchCategoryFailure(error));
      });
  };
};

export default fetchCategories;
