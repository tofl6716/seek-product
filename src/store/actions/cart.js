import * as actionTypes from "./actionTypes";
import {
  fetchCartAPI,
  addToCartAPI,
  removeFromCartAPI,
  updateCartAPI,
  cartPaymentAPI
} from "api/cart";

// Fetch user cart

const fetchCartPending = () => {
  return {
    type: actionTypes.FETCH_CART_PENDING
  };
};

const fetchCartSuccess = cart => {
  return {
    type: actionTypes.FETCH_CART_SUCCESS,
    payload: cart
  };
};

const fetchCartFailure = error => {
  return {
    type: actionTypes.FETCH_CART_FAILURE,
    payload: error
  };
};

export const fetchCart = token => {
  return dispatch => {
    dispatch(fetchCartPending());

    fetchCartAPI(token)
      .then(res => {
        dispatch(fetchCartSuccess(res.data));
      })
      .catch(error => {
        dispatch(fetchCartFailure(error));
      });
  };
};

// Add to cart

const addToCartPending = () => {
  return {
    type: actionTypes.ADD_TO_CART_PENDING
  };
};

const addToCartSuccess = cartDetail => {
  return {
    type: actionTypes.ADD_TO_CART_SUCCESS,
    payload: cartDetail
  };
};

const addToCartFailure = error => {
  return {
    type: actionTypes.ADD_TO_CART_FAILURE,
    payload: error
  };
};

export const addToCart = (token, productId, amount) => {
  return dispatch => {
    dispatch(addToCartPending());

    addToCartAPI(token, productId, amount)
      .then(res => {
        if (res.status === 200) dispatch(addToCartSuccess(res.data));
      })
      .catch(error => {
        dispatch(addToCartFailure(error));
      });
  };
};

// Remove from cart

const removeFromCartPending = () => {
  return {
    type: actionTypes.REMOVE_FROM_CART_PENDING
  };
};

const removeFromCartSuccess = cart => {
  return {
    type: actionTypes.REMOVE_FROM_CART_SUCCESS,
    payload: cart
  };
};

const removeFromCartError = error => {
  return {
    type: actionTypes.REMOVE_FROM_CART_FAILURE,
    payload: error
  };
};

export const removeFromCart = (token, productId) => {
  return dispatch => {
    dispatch(removeFromCartPending());

    // Call API
    removeFromCartAPI(token, productId)
      .then(res => {
        dispatch(removeFromCartSuccess(res.data));
      })
      .catch(error => {
        dispatch(removeFromCartError(error));
      });
  };
};

// Update cart

const updateCartPending = () => {
  return {
    type: actionTypes.UPDATE_CART_PENDING
  };
};

const updateCartSuccess = cart => {
  return {
    type: actionTypes.UPDATE_CART_SUCCESS,
    payload: cart
  };
};

const updateCartFailure = error => {
  return {
    type: actionTypes.UPDATE_CART_FAILURE,
    payload: error
  };
};

export const updateCart = (token, productId, amount) => {
  return dispatch => {
    dispatch(updateCartPending());
    // Call API
    updateCartAPI(token, productId, amount)
      .then(res => {
        dispatch(updateCartSuccess(res.data));
      })
      .catch(error => {
        dispatch(updateCartFailure(error));
      });
  };
};

// Cart payment

const cartPaymentPending = () => {
  return {
    type: actionTypes.CART_PAYMENT_PENDING
  };
};

const cartPaymentSuccess = () => {
  return {
    type: actionTypes.CART_PAYMENT_SUCCESS
  };
};

const cartPaymentFailure = error => {
  return {
    type: actionTypes.CART_PAYMENT_FAILURE,
    payload: error
  };
};

export const cartPayment = (userToken, stripeToken) => {
  return dispatch => {
    dispatch(cartPaymentPending());
    // Call API
    cartPaymentAPI(userToken, stripeToken)
      .then(res => {
        // Payment successfully
        if (res.data.message === "SUCCESSFUL!!!") {
          dispatch(cartPaymentSuccess());
          dispatch(fetchCart(userToken));
        }
      })
      .catch(error => {
        dispatch(cartPaymentFailure(error));
      });
  };
};
