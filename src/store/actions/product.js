import {
  getRandomProductsFromAPI,
  getProductsByCategoryFromAPI,
  productDetailsAPI
} from "api/product";
import * as actionTypes from "./actionTypes";

/* ========== FETCH RANDOM PRODUCTS ========== */
const fetchRandomProductsPending = () => {
  return {
    type: actionTypes.FETCH_RANDOM_PRODUCTS_PENDING
  };
};

const fetchRandomProductsSuccess = products => {
  return {
    type: actionTypes.FETCH_RANDOM_PRODUCTS_SUCCESS,
    payload: products
  };
};

const fetchRandomProductsFailure = error => {
  return {
    type: actionTypes.FETCH_RANDOM_PRODUCTS_FAILURE,
    payload: error
  };
};

export const fetchRandomProducts = () => {
  return dispatch => {
    dispatch(fetchRandomProductsPending());
    getRandomProductsFromAPI()
      .then(res => {
        dispatch(fetchRandomProductsSuccess(res.data));
      })
      .catch(error => {
        dispatch(fetchRandomProductsFailure(error));
      });
  };
};
/* ========== FETCH RANDOM PRODUCTS ========== */



/* ========== FETCH PRODUCTS BY CATEGORY ========== */
const fetchProductsByCategoryPending = () => {
  return {
    type: actionTypes.FETCH_PRODUCTS_BY_CATEGORY_PENDING
  };
};

const fetchProductsByCategorySuccess = products => {
  return {
    type: actionTypes.FETCH_PRODUCTS_BY_CATEGORY_SUCCESS,
    payload: products
  };
};

const fetchProductsByCategoryFailure = error => {
  return {
    type: actionTypes.FETCH_PRODUCTS_BY_CATEGORY_FAILURE,
    payload: error
  };
};

export const fetchProductsByCategory = category => {
  return dispatch => {
    dispatch(fetchProductsByCategoryPending());
    getProductsByCategoryFromAPI(category)
      .then(res => {
        dispatch(fetchProductsByCategorySuccess(res.data.results));
      })
      .catch(error => {
        dispatch(fetchProductsByCategoryFailure(error));
      });
  };
};
/* ========== FETCH PRODUCTS BY CATEGORY ========== */



/* ========== FETCH PRODUCT DETAILS ========== */
const fetchProductDetailsPending = () => {
  return {
    type: actionTypes.FETCH_PRODUCT_DETAILS_PENDING
  };
};

const fetchProductDetailsSuccess = (product) => {
  return {
    type: actionTypes.FETCH_PRODUCT_DETAILS_SUCCESS,
    payload: product
  };
};

const fetchProductDetailsFailer = (error) => {
  return {
    type: actionTypes.FETCH_PRODUCT_DETAILS_FAILURE,
    payload: error
  }
};

export const fetchProductDetails = (productId) => {
  return dispatch => {
    dispatch(fetchProductDetailsPending());

    productDetailsAPI(productId)
    .then(res => {
      dispatch(fetchProductDetailsSuccess(res.data));
    })
    .catch(error => {
      dispatch(fetchProductDetailsFailer(error));
    })
  }
}