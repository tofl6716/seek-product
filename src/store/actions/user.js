import * as actionTypes from './actionTypes';
import { getProfileFromAPI, updateUserProfileAPI } from 'api/user';


/* ========== FETCH USER PROFILE ========== */
const fetchUserProfilePending = () => {
  return {
    type: actionTypes.FETCH_USER_PROFILE_PENDING
  };
};

const fetchUserProfileSuccess = (user) => {
  return {
    type: actionTypes.FETCH_USER_PROFILE_SUCCESS,
    payload: user
  };
};

const fetchUserProfileFailure = (error) => {
  return {
    type: actionTypes.FETCH_USER_PROFILE_FAILURE,
    payload: error
  };
};

// Main action
export const fetchUserProfile = (token) => {
  return dispatch => {
    dispatch(fetchUserProfilePending());

    getProfileFromAPI(token)
      .then(res => {
        dispatch(fetchUserProfileSuccess(res.data));
      })
      .catch(error => {
        dispatch(fetchUserProfileFailure(error));
      });
  };
};
/* ========== FETCH USER PROFILE ========== */


/* ========== UPDATE USER PROFILE ========== */
const updateUserProfilePending = () => {
  return {
    type: actionTypes.UPDATE_USER_PROFILE_PENDING
  };
};

const updateUserProfileSuccess = (body) => {
  return {
    type: actionTypes.UPDATE_USER_PROFILE_SUCCESS,
    payload: body
  };
};

const updateUserProfileFailure = (error) => {
  return {
    type: actionTypes.UPDATE_USER_PROFILE_FAILURE,
    payload: error
  }
}

export const updateUserProfile = (token, body) => {
  return dispatch => {
    dispatch(updateUserProfilePending());

    updateUserProfileAPI(token, body)
      .then(res => {
        if (res.data.message === "PROFILE_UPDATED") {
          dispatch(updateUserProfileSuccess(body));
        }
      })
      .catch(error => {
        dispatch(updateUserProfileFailure());
      });
  };
};
/* ========== UPDATE USER PROFILE ========== */



/* ========== REMOVE USER FROM THE STORE AFTER SIGNING OUT ========== */
export const removeUser = () => {
  return {
    type: actionTypes.REMOVE_USER
  };
};
/* ========== REMOVE USER FROM THE STORE AFTER SIGNING OUT ========== */
