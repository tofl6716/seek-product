import * as actionTypes from "../actions/actionTypes";

const initialState = {
  cart: null,
  fetchPending: false,
  fetchError: null,
  addToCartPending: false,
  addToCartSuccess: false,
  addToCartError: null,
  removeFromCartPending: false,
  removeFromCartError: null,
  updateCartPending: false,
  updateCartError: null,
  paymentPending: false,
  paymentSuccess: false,
  paymentError: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    // --- Fetch cart --- //
    case actionTypes.FETCH_CART_PENDING:
      return {
        ...state,
        cart: null,
        fetchPending: true,
        fetchError: null
      };
    case actionTypes.FETCH_CART_SUCCESS:
      return {
        ...state,
        cart: action.payload,
        fetchPending: false,
        error: null
      };
    case actionTypes.FETCH_CART_FAILURE:
      return {
        ...state,
        cart: null,
        fetchPending: false,
        error: action.payload
      };
    // --- Add to cart --- //
    case actionTypes.ADD_TO_CART_PENDING:
      return {
        ...state,
        addToCartPending: true,
        addToCartSuccess: false,
        addToCartError: null
      };
    case actionTypes.ADD_TO_CART_SUCCESS:
      return {
        ...state,
        cart: { ...state.cart, ...action.payload },
        addToCartPending: false,
        addToCartSuccess: true,
        addToCartError: null
      };
    case actionTypes.ADD_TO_CART_FAILURE:
      return {
        ...state,
        addToCartPending: false,
        addToCartSuccess: false,
        addToCartError: action.payload
      };
    // --- Remove from cart --- //
    case actionTypes.REMOVE_FROM_CART_PENDING:
      return {
        ...state,
        removeFromCartPending: true,
        removeFromCartError: null
      };
    case actionTypes.REMOVE_FROM_CART_SUCCESS:
      return {
        ...state,
        cart: { ...state.cart, ...action.payload },
        removeFromCartPending: false,
        removeFromCartError: null
      };
    case actionTypes.REMOVE_FROM_CART_FAILURE:
      return {
        ...state,
        removeFromCartPending: false,
        removeFromCartError: action.payload
      };
    // ----- Update cart ----- //
    case actionTypes.UPDATE_CART_PENDING:
      return {
        ...state,
        updateCartPending: true,
        updateCartError: null
      };
    case actionTypes.UPDATE_CART_SUCCESS:
      return {
        ...state,
        cart: { ...state.cart, ...action.payload },
        updateCartPending: false,
        updateCartError: null
      };
    case actionTypes.UPDATE_CART_FAILURE:
      return {
        ...state,
        updateCartPending: false,
        updateCartError: action.payload
      };
    // ----- Cart payment ----- //
    case actionTypes.CART_PAYMENT_PENDING:
      return {
        ...state,
        paymentPending: true,
        paymentSuccess: false,
        paymentError: null
      };
    case actionTypes.CART_PAYMENT_SUCCESS:
      return {
        ...state,
        paymentPending: false,
        paymentSuccess: true,
        paymentError: null
      };
    case actionTypes.CART_PAYMENT_FAILURE:
      return {
        ...state,
        paymentPending: false,
        paymentSuccess: false,
        paymentError: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
