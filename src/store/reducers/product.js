import * as actionTypes from "../actions/actionTypes";

const initialState = {
  products: [],
  productDetails: null,
  pending: false,
  error: null,
  productDetailPending: false,
  productDetailError: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    // Fetch random products and products by category
    case actionTypes.FETCH_RANDOM_PRODUCTS_PENDING:
    case actionTypes.FETCH_PRODUCTS_BY_CATEGORY_PENDING:
      return {
        ...state,
        pending: true
      };

    case actionTypes.FETCH_RANDOM_PRODUCTS_SUCCESS:
    case actionTypes.FETCH_PRODUCTS_BY_CATEGORY_SUCCESS:
      return {
        ...state,
        products: action.payload,
        pending: false
      };

    case actionTypes.FETCH_CATEGORIES_FAILURE:
    case actionTypes.FETCH_PRODUCTS_BY_CATEGORY_FAILURE:
      return {
        ...state,
        error: action.payload,
        pending: false
      };
    // Fetch product details
    case actionTypes.FETCH_PRODUCT_DETAILS_PENDING:
      return {
        ...state,
        productDetails: null,
        productDetailPending: true,
        productDetailError: null
      };
    case actionTypes.FETCH_PRODUCT_DETAILS_SUCCESS:
      return {
        ...state,
        productDetails: action.payload,
        productDetailPending: false,
        productDetailError: null
      };
    case actionTypes.FETCH_PRODUCT_DETAILS_FAILURE:
      return {
        ...state,
        productDetails: null,
        productDetailPending: false,
        productDetailError: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
