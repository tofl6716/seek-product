import * as actionTypes from '../actions/actionTypes';

const initialState = {
  user: null,
  fetchPending: false,
  fetchError: null,
  updatePending: false,
  updateError: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_USER_PROFILE_PENDING:
      return {
        ...state,
        fetchPending: true,
        fetchError: null
      };
    case actionTypes.FETCH_USER_PROFILE_SUCCESS:
      return {
        ...state,
        user: action.payload,
        fetchPending: false,
        fetchError: null
      };
    case actionTypes.FETCH_USER_PROFILE_FAILURE:
      return {
        ...state,
        user: null,
        fetchPending: false,
        fetchError: action.payload
      };
    case actionTypes.UPDATE_USER_PROFILE_PENDING:
      return {
        ...state,
        updatePending: true,
        updateError: null
      };
    case actionTypes.UPDATE_USER_PROFILE_SUCCESS:
      return {
        ...state,
        user: {...state.user, ...action.payload},
        updatePending: false,
        updateError: null,
      };
    case actionTypes.UPDATE_USER_PROFILE_FAILURE:
      return {
        ...state,
        updatePending: false,
        updateError: action.error
      };
    case actionTypes.REMOVE_USER:
      return {
        ...state,
        user: null
      }
    default:
      return state;
  }
}

export default reducer;