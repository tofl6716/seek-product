import * as actionTypes from "../actions/actionTypes";

const initialState = {
  categories: [],
  pending: false,
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_CATEGORIES_PENDING:
      return {
        ...state,
        pending: true
      };
    case actionTypes.FETCH_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: [...action.payload],
        pending: false,
        error: null
      };
    case actionTypes.FETCH_CATEGORIES_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      };
    default:
      return state;
  }
};

export default reducer;
