import * as actionTypes from "../actions/actionTypes";

const initalState = {
  token: "",
  signinPending: false,
  signinError: null,
  signoutPending: false,
  signoutError: null,
  signupPending: false,
  signupSuccess: false,
  signupError: null,
};

const reducer = (state = initalState, action) => {
  switch (action.type) {
    case actionTypes.SIGNIN_PENDING:
      return {
        ...state,
        token: "",
        signinPending: true,
        signinError: null
      };
    case actionTypes.SIGNIN_SUCCESS:
      localStorage.setItem("token", action.payload);
      return {
        ...state,
        token: action.payload,
        signinPending: false,
        signinError: null
      };
    case actionTypes.SIGNIN_FAILURE:
      return {
        ...state,
        token: "",
        signinPending: false,
        signinError: action.payload
      };

    case actionTypes.SIGNOUT_PENDING:
      return {
        ...state,
        signoutPending: true,
        signoutError: null
      };
    case actionTypes.SIGNOUT_SUCCESS:
      localStorage.removeItem("token");
      return {
        ...state,
        token: "",
        signoutPending: false,
        signoutError: null
      };
    case actionTypes.SIGNOUT_FAILURE:
      return {
        ...state,
        signoutPending: false,
        signoutError: action.payload
      };
    case actionTypes.SIGNUP_PENDING:
      return {
        ...state,
        signupPending: true,
        signupError: null
      };
    case actionTypes.SIGNUP_SUCCESS:
      return {
        ...state,
        signupPending: false,
        signupError: null,
        signupSuccess: true,
      };
    case actionTypes.SIGNUP_FAILURE:
      return {
        ...state,
        signupPending: false,
        signupError: action.payload
      };
    case actionTypes.SET_TOKEN:
      return {
        ...state,
        token: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
