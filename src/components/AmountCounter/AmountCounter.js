import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";

const AmountCounter = props => {
  return (
    <div className="input-group">
      <div className="input-group-prepend">
        <button className="btn btn-secondary" onClick={props.decrement} disabled={props.disabled}>
          <FontAwesomeIcon icon={faMinus} />
        </button>
      </div>
      <input
        type="text"
        style={{ maxWidth: 70 }}
        className="form-control text-center font-weight-bold"
        onChange={props.change}
        value={props.value}
        defaultValue={props.defaultValue}
        disabled={props.disabled}
        readOnly
      />
      <div className="input-group-prepend">
        <button className="btn btn-secondary" onClick={props.increment} disabled={props.disabled}>
          <FontAwesomeIcon icon={faPlus} />
        </button>
      </div>
    </div>
  );
};

export default AmountCounter;
