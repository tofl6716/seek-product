import React from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";

const CategoryList = ({ categories }) => {
  categories.sort((a, b) => {
    const nameA = a.name.charAt(0).toUpperCase();
    const nameB = b.name.charAt(0).toUpperCase();
    if (nameA < nameB) {
      return -1;
    } else if (nameA > nameB) {
      return 1;
    }
    return 0;
  }
  
  );
  return (
    <div className="flex-shrink-0 border-right">
      <p><b>Category</b></p>
      <ul className="nav nav-pills flex-column" style={{minWidth: 300}}>
        {categories.map(({ name, id }) => {
          const link = name.toLowerCase().replace(/( )+/g, '-');
          return (
            <li className="nav-item" key={id}>
              <NavLink className="nav-link" to={`/${link}`}>{name}</NavLink>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

CategoryList.propTypes = {
  categories: PropTypes.array.isRequired
}

export default CategoryList;
