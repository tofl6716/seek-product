import React from 'react';
import PropTypes from "prop-types";

const Alert = ({ children, type = 'primary' }) => {
  return (
    <div className={`alert alert-${type}`}>
      {children}
    </div>
  );
};

Alert.propTypes = {
  children: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark'])
};

export default Alert;