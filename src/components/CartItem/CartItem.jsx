import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import AmountCounter from 'components/AmountCounter/AmountCounter';
import Spinner from 'components/Spinner/Spinner';

const CartItem = ({ detail, onRemoveItem, deletePending, onDecrement, onIncrement, updateCartPending }) => {
  return (
    <>
      <div className="row shadow rounded mb-4 py-3 mx-1">
        <div className="col-sm-2">
          <img src={detail.product.image || 'https://via.placeholder.com/400x300'} alt="" className="img-fluid" />
        </div>

        <div className="col-sm-8">
          <p className="text-truncate"><Link to={`/product/${detail.product.id}`}>{detail.product.name}</Link></p>
          <p>Price: ${detail.product.price}</p>
          <button className="btn btn-outline-danger btn-sm">
            <Spinner show={deletePending} />
            {
              deletePending ?
                <span>Deleting...</span> :
                <>
                  <FontAwesomeIcon icon={faTimes} />
                  <span className="ml-1" onClick={() => onRemoveItem(detail.product.id)}>
                    Delete
                  </span>
                </>
            }

          </button>
        </div>

        <div className="col">
          <AmountCounter
            disabled={updateCartPending}
            value={detail.amount}
            decrement={() => onDecrement(detail.product.id, detail.amount - 1)}
            increment={() => onIncrement(detail.product.id, detail.amount + 1)}
          />
          <h5 className="text-danger mt-2">Total: ${detail.total}</h5>
        </div>
      </div>
    </>
  )
}

CartItem.propTypes = {
  detail: PropTypes.object.isRequired,
  onRemoveItem: PropTypes.func.isRequired,
  onDecrement: PropTypes.func.isRequired,
  onIncrement: PropTypes.func.isRequired,
  deletePending: PropTypes.bool,
  updateCartPending: PropTypes.bool
}

export default CartItem;