import React from 'react';
import PropTypes from 'prop-types';
import './Product.css';

const Product = ({ id, image, name, price }) => {
  return (
    <div className="product" >
      <a href={`/product/${id}`} className="text-decoration-none product-info" title={name}>
        <div className="image">
          <img src={image || 'http://via.placeholder.com/400x300'} alt={name} />
        </div>
        <div className="name">{name}</div>
        <div className="price">${price}</div>
      </a>
    </div>
  )
}

Product.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired
};

export default Product;