import React from 'react';
import PropTypes from 'prop-types';
import './ProductList.css';

import Product from './Product/Product';

const ProductList = ({ products }) => {
  return (
    <div className="product-side">
      <div className="product-list">
        {
          products.length ?
            products.map(({ name, id, image, price }) => {
              return <Product key={id} name={name} id={id} image={image} price={price} />
            }) : <p>There is no product to display, please choose another category.</p>
        }
      </div>
    </div>
  )
}

ProductList.propTypes = {
  products: PropTypes.array.isRequired
};

export default ProductList;