import React from 'react';
import PropTypes from 'prop-types';

const Spinner = ({ show = true, variant }) => {
  return (
    show ?
      <span className={`spinner-border spinner-border-sm mr-2 text-${variant}`}></span>
      : null
  );
};

Spinner.propTypes = {
  show: PropTypes.bool,
  variant: PropTypes.string
};

export default Spinner;