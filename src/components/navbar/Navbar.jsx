import React from "react";
import { NavLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart, faSignOutAlt, faSignInAlt, faUser, faUserPlus } from '@fortawesome/free-solid-svg-icons';

import logo from 'assets/logo.svg';
import './Navbar.css';

const Navbar = ({ user, signoutClick, location, cart }) => {
  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
      <NavLink className="navbar-brand" to="/">
        <img height="30" src={logo} alt="" />
      </NavLink>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse ml-auto" id="navbarNav">
        <ul className="navbar-nav ml-auto">
          {
            user ?
              <>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/cart">
                    <FontAwesomeIcon icon={faShoppingCart} />
                    <span className="ml-1">Cart ({cart ? cart.cart_detail.length : null})</span>
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/me">
                    <FontAwesomeIcon icon={faUser} />
                    <span className="ml-1">{`${user.first_name} ${user.last_name}`}</span>
                  </NavLink>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/sign-out" onClick={signoutClick}>
                    <FontAwesomeIcon icon={faSignOutAlt} />
                    <span className="ml-1">Sign out</span>
                  </a>
                </li>
              </>
              :
              <>
                <li className="nav-item">
                  <NavLink className="nav-link" to={{
                    pathname: "/sign-in",
                    state: { from: location }
                  }}>
                    <FontAwesomeIcon icon={faSignInAlt} />
                    <span className="ml-1">Sign in</span>
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sign-up">
                    <FontAwesomeIcon icon={faUserPlus} />
                    <span className="ml-1">Sign up</span>
                  </NavLink>
                </li>
              </>
          }
        </ul>
      </div>
    </nav>
  );
};

Navbar.propTypes = {
  user: PropTypes.object
};

export default withRouter(Navbar);
