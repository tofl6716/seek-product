import axios from "axios";

const instance = axios.create({
  baseURL: "http://api.seekproduct.com"
});

export default instance;