import axios from './axios';

export const getProfileFromAPI = (token) => {
  return axios.get('/api/auth/profile', {
    headers: {
      Authorization: 'JWT ' + token
    }
  });
}


export const updateUserProfileAPI = (token, body) => {
  return axios.put('/api/auth/profile', body, {
    headers: {
      Authorization: 'JWT ' + token
    }
  })
};
