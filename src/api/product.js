import axios from './axios';

export const getRandomProductsFromAPI = () => {
  return axios.get("/api/products/random_products");
};

export const getProductsByCategoryFromAPI = categoryName => {
  const body = {
    'key_word': '',
    'category': categoryName
  };
  return axios.post("/api/category/search/?order_by=-id", body);
};

export const productDetailsAPI = productId => {
  return axios.get(`/api/products/${productId}/details`);
}