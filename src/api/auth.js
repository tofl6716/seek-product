import axios from "./axios";

const USER_API_URL = "/user";
const AUTH_API_URL = "/api/auth";

export const signin = (email, password) => {
  const body = {
    email,
    password
  };

  return axios.post(USER_API_URL + "/login", body);
};

export const signout = token => {
  return axios.get(USER_API_URL + "/logout", {
    headers: {
      Authorization: "JWT " + token
    }
  });
};

export const signup = (
  firstName,
  lastName,
  email,
  password,
  confirmPassword
) => {
  return axios.post(AUTH_API_URL + "/register", {
    first_name: firstName,
    last_name: lastName,
    email,
    password,
    confirm_password: confirmPassword
  });
};
