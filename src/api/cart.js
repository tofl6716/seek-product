import axios from "./axios";

const CART_URL = "/api/user/cart";

export const fetchCartAPI = token => {
  return axios.get(CART_URL + "/view-cart", {
    headers: {
      Authorization: "JWT " + token
    }
  });
};

export const addToCartAPI = (token, productId, amount) => {
  return axios.post(
    CART_URL + "/add-to-cart",
    {
      product: productId,
      amount: amount
    },
    {
      headers: {
        Authorization: "JWT " + token
      }
    }
  );
};

export const removeFromCartAPI = (token, productId) => {
  return axios.post(
    CART_URL + "/remove-from-cart",
    {
      product: productId
    },
    {
      headers: {
        Authorization: "JWT " + token
      }
    }
  );
};

export const updateCartAPI = (token, productId, amount) => {
  return axios.post(
    CART_URL + "/update-cart",
    {
      product: productId,
      amount
    },
    {
      headers: {
        Authorization: "JWT " + token
      }
    }
  );
};

export const cartPaymentAPI = (token, stripeToken) => {
  return axios.post(
    CART_URL + "/payment",
    {
      option: 2,
      token: stripeToken
    },
    {
      headers: {
        Authorization: "JWT " + token
      }
    }
  );
};
