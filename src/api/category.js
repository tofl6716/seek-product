import axios from './axios';

export const getCategoriesFromAPI = () => {
  return axios.get("/api/category/list");
};