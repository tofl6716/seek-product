import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { fetchUserProfile } from 'store/actions/user';
import { setToken } from 'store/actions/auth';

// Import redux stuff
import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";

// Import reducers
import categoryReducer from "store/reducers/category";
import productReducer from "store/reducers/product";
import authReducer from "store/reducers/auth";
import userReducer from "store/reducers/user";
import cartReducer from "store/reducers/cart";
import { fetchCart } from "store/actions/cart";

/**
 * Add Redux devtools only in development mode
 * and combine all reducers as rootReducers
*/
const composeEnhancers = (process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null) || compose;
const rootReducer = combineReducers({
  category: categoryReducer,
  product: productReducer,
  auth: authReducer,
  user: userReducer,
  cart: cartReducer
});

// Create redux store
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);


// Try to login with token stored on localStorage
const localStorageToken = localStorage.getItem('token');
if (localStorageToken) {
  store.dispatch(fetchUserProfile(localStorageToken));
  store.dispatch(setToken(localStorageToken));
  store.dispatch(fetchCart(localStorageToken));
};


ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
