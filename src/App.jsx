import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { connect } from 'react-redux';
import { StripeProvider } from 'react-stripe-elements';

import Navbar from "components/navbar/Navbar";
import Home from "pages/home/HomeContainer";
import Cart from "pages/cart/CartContainer";
import Signin from "pages/signin/SigninContainer";
import Signup from "pages/signup/SignupContainer";
import Profile from "pages/profile/ProfileContainer";

import { signoutRequest } from 'store/actions/auth';
import ProductDetailContainer from "pages/product-detail/ProductDetailContainer";
import CheckoutContainer from "pages/checkout/CheckoutContainer";

class App extends React.Component {

  handleSignout = (e) => {
    e.preventDefault();
    const { signout, token } = this.props;
    const confirmation = window.confirm("Do you want to sign out?");
    if (confirmation) {
      signout(token);
    };
  }

  render() {
    return (
      <StripeProvider apiKey="pk_test_7X4at47jVmUqka7N8HhdO35N">
        <BrowserRouter>
          <Navbar signoutClick={this.handleSignout} user={this.props.user} cart={this.props.cart} />
          <Switch>
            <Route path="/cart" component={Cart} />
            <Route path="/me" component={Profile} />
            <Route path="/sign-in" component={Signin} />
            <Route path="/sign-up" component={Signup} />
            <Route path="/check-out" component={CheckoutContainer} />
            <Route path="/" exact component={Home} />
            <Route path="/product/:productId" component={ProductDetailContainer} />
            <Route path="/:category" component={Home} />
          </Switch>
        </BrowserRouter>
      </StripeProvider>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user.user,
    token: state.auth.token,
    cart: state.cart.cart
  }
}

const mapDispatchToProps = dispatch => {
  return {
    signout: (token) => dispatch(signoutRequest(token))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
